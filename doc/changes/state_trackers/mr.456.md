OpenXR: Fix multiplicity of bounds paths per action - there's one per input/output.
