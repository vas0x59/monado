---
- mr.427
---
Generalize handling of native-platform handles in IPC code, allow bi-directional handle transfer, and de-duplicate code between server and client.
