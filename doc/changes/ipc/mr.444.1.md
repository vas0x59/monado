generation: Fix handling 'in_handle' by adding a extra sync round-trip, this
might be solvable by using `SOCK_SEQPACKET`.
